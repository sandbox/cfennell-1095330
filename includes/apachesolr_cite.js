$(document).ready(function(){
	
	
	$(".cite-database-keywords ul").each(function(){
	  var height = $(this).height();
		if (height > 20) {
			$(this).addClass('keyword-expand');
		}
	});
	
	//if we don't test to ensure this is defined, all hell breaks loose
	//of course, I'm not concerned about all hell breaking loose, but that a PART of hell will break loose... it'll be much harder to detect.

	$(".cite-database-keywords-box").mouseover(
		function() {
			var height = $(this).children('.cite-database-keywords').children('ul').height();
			if (height > 20) {
				$(this).children('.cite-database-keywords-expanded').show();
	  		$(this).children('.cite-database-keywords').children('ul').removeClass('keyword-expand');
	  		$(this).children('.cite-database-keywords').children('ul').addClass('keyword-unexpand');
			}
		}
	);
	$(".cite-database-keywords-box").mouseout(
		function() {			
			var height = $(this).children('.cite-database-keywords').children('ul').height();
			if (height > 20) {
				$(this).children('.cite-database-keywords-expanded').hide();				
	  			$(this).children('.cite-database-keywords').children('ul').addClass('keyword-expand');
	  			$(this).children('.cite-database-keywords').children('ul').removeClass('keyword-unexpand');
			}
		}
	);
		
	$(".basic-search-box :input.form-submit").hover(
		function() {
			$(this).attr('src', 'https://www.ethicshare.org/sites/all/themes/ethicshare/images/search_button_active.png');
		},
		function() {
			$(this).attr('src', 'https://www.ethicshare.org/sites/all/themes/ethicshare/images/search_button.png');
		}		
	);

	if (typeof(_GBSBookInfo) !== 'undefined') {
	  var buttonImg = 'https://code.google.com/apis/books/images/gbs_preview_button1.gif';
	  for (isbn in _GBSBookInfo) {
	    var b = _GBSBookInfo[isbn];
	    if (b && (b.preview == "full" || b.preview == "partial")) {
					//colon is a reserved character, so we replace it
	  			var div_id = '#'+ isbn.replace(/:/, "-");
	  			$(div_id).html('<a href="' + b.preview_url + '"><img '+ 'src="' + buttonImg + '" '+ 'style="border:0; margin:3px;" /><\/a>');
	      break;
	    }
	  }
	}

  $('[name=sortitselect]').change(submit_select);
	
	//always default to the new folder - keeps screwy things from happening when user navigates off the search page
	$(":input.cite-folder-submit-dropdown").val(0);
	$(":input.cite-folder-submit-dropdown").change(og_folder_alter);
	
 	//handle the checkall checkbox
	$(":checkbox#edit-checkall").click(apachesolr_cite_checkall);
	
	
	//AJAX Handler for saving to folders
	$(":input.folder-search-submit").click(function(){
		if ($('#edit-folder-nid').val() != 0) {
			save_to_folder_non_cache();
			return false;
		}
	});
	
	$(":input.folder-search-submit").click(function(){
		if ($('#edit-folder-nid').val() == 0) {
			jQuery.each(jQuery.browser, function(i, val) {
				if (i == 'msie' && val == true) {
					var nids = '';
					$("input[type=checkbox][checked]:not([name=checkall])").each(						
					  function() {
							if (this.checked == true) {
								nids = nids + "&nids[]="+ this.value;
							}
					  }
					);
					$("#apachesolr-cite-search-results-form").attr('action',  Drupal.settings.baseUrl + '/node/add/og-folder?' + nids.substr(1) );										
					$("#apachesolr-cite-search-results-form").submit();
				}
			});
		}
	});
	
	
	//AJAX Handler for saving to folders
	// $(":input.folder-search-submit").click(function(){
	// 	if ($('#edit-folder-nid').val() != 0) {
	// 		save_to_folder_multiple("apachesolr_cite_search_cache");
	// 		return false;
	// 	}
	// });
	
	// $(":checkbox.search-result-checkbox").change(function(){
	// 	og_folder_cache_single(this.value, this.checked, "apachesolr_cite_search_cache");		
	// 	return false;
	// });
});

function submit_select() {
	var path = Drupal.settings.baseUrl + this.value;
	window.location  = path;
}

//set the form action to self unless we have a new folder
function og_folder_alter() {
	//set the selected value for the given item
	// $(":input#edit-folder-nid").val(this.value)
	// $("#edit-folder-nid  option[value='" + this.value +"']").attr("selected", "selected");	
 // $(":input#edit-folder-nid  option[value='" + this.value +"']").attr("selected", "selected");

	$(":input#edit-folder-nid option[value='" + this.value +"']").attr("selected", "selected");
	if (this.value != 0) {
		$("#apachesolr-cite-search-results-form").attr("target", "_self");
	}
	else {
		$("#apachesolr-cite-search-results-form").attr("target", "_blank");
	}
}


function apachesolr_cite_checkall() {
	var checked_status = this.checked;
	$(':checkbox.checkall').each(function() {
		this.checked = checked_status;
	});
	var cite_nids_delete = new Array();
	var cite_nids_add = new Array();
	var x = 0;
	$(':checkbox.checkall').each(function() {
		if (this.checked == true) {
			cite_nids_add[x] = this.value;
		}
		else {
			cite_nids_delete[x] = this.value;
		}
		x++;		
	});
	// phasing out checkbox caching
	// var post_path = Drupal.settings.baseUrl + "/og_folders/checkbox_cache/ajax";
	// if (cite_nids_add.length) {
	// 	$.post(post_path, {'nids[]': cite_nids_add, 'op': 'add', 'namespace': 'apachesolr_cite_search_cache'},
	// 					function (data) {
	// 						if (data) {
	// 							// alert(data);
	// 						}
	// 				  }
	// 				);
	// }
	// if (cite_nids_delete.length) {
	// 	$.post(post_path, {'nids[]': cite_nids_delete, 'op': 'remove'});	
	// }
}