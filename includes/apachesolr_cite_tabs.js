$(function() {
		//init overlabel for page load: show overlabel for keyword box
		$("#header-bottom label").overlabel();

	
		$("a.keyword-link").click(function(){ 	
			$("#header-bottom-last label.overlabel").hide();
			$("div.search-hydra-container-basic").find("#search-box-basic:hidden").slideDown("fast"); 
			// Fixes a quirk in Chrome - need to hide the label first and then fade-in
			$("#header-bottom label").overlabel().hide();
			$("#header-bottom label").overlabel().fadeIn('slow');						
			$("div.search-hydra-container-advanced").find("div.advanced-search-box:visible").slideUp("fast");
			$('#header-bottom-last').slideUp("fast");
			$("div.search-toggle-basic").find('div.advanced-link').show();
			$("div.search-toggle-advanced").find('div.keyword-link').hide();
						
		});

		$("a.advanced-link").click(function(){ 
			$('#header-bottom-last').show();
			$("div.search-hydra-container-advanced").find("div.advanced-search-box:hidden").slideDown("fast");
			$("div.search-hydra-container-basic").find("#search-box-basic:visible").slideUp("fast");
			$("div.search-toggle-advanced").find('div.keyword-link').show();
			$("div.search-toggle-basic").find('div.advanced-link').hide();
			$("#header-bottom label").hide()
			$("#header-bottom-last label.overlabel").overlabel().fadeIn('slow');	
		});		
 });
