<?php
global $user;

if ($variables['found_results']) {
  $output .= sprintf('<div id="search-header"><div class="apachesolr-count">%s</div> <div class="apachesolr-to-show">%s</div> <div class="search-sort">%s</div></div><div class="clear"></div>', $count_text, $to_show, $sort);

  // $output .= ('<script type="text/javascript" src="http://books.google.com/books/previewlib.js"></script>');
  $output .= '<div class="clear"></div>';  
  
  if (str_replace('<div id="refinements"><div class="item-list"></div></div>', '', $filters)) {
   $search_header_top .= sprintf('<div class="search-results-bar top">%s</div>', $filters);
  }

    // Commenting out for now to see if new checkboxes on side work just as well (this will save us some page clutter)
    // if ($filters) {
      // $output  .= sprintf('<div class="search-results-bar top"><div id="refinements">%s</div></div><div class="clear"></div>', $filters);
    // }
  
  $output .= '<div class="folder-messages"></div><div class="clear"></div>';
  
  //folder actions- only for logged-in users
  if ($user->uid >= 1) {
    $folders_form  .= sprintf('<div id="save-to-folder"><table id="search-add-to-folder"><tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr></table></div>', t('Save:'), 
      $folder, $form_submit, t('Export:'), $export_dropdown, $export_submit);  
    $folders_links  = sprintf('<div class="folder-view-links">%s | %s</div>', l('My Folders', 'folders/my'), l('All Folders', 'folders'));      
  }
  else {
    $folders_form  .= sprintf('<div id="save-to-folder"><table id="search-add-to-folder"><tr><td>%s</td><td>%s</td><td>%s</td></tr></table></div>', 
      t('Export to:'), $export_dropdown, $export_submit);  
  }    

  if ($_REQUEST['filters']) {
    $header_sticky .= sprintf('<div class="search-results-bar top">%s</div><div class="clear"></div>', $filter_breadcrumb);
  }

  $header_sticky .= sprintf('<div class="search-results-bar bottom"> %s</div>', $folders_form);
  $header_sticky .= '<div class="clear"></div>';      
  $header_sticky .= '<div class="clear"></div>';
  $header_sticky .= sprintf('<div id="checkall"><table id="checkall-table"><tr><td class="checkall-input-col">%s</td><td class="checkall-label-col">%s</td></tr></table><table id="folder-links-table"><tr><td>%s</td></tr></table></div>', 
  $form_checkall, t('Select All'), $folders_links);
  
  //search results
  $search_results .= $form_citation_checkboxes;
  $search_results .= '<div class="clear"></div>';
  $search_results .= $form_op;
  $search_results .= $form_destination;
  $search_results .= $form;  
  $search_results = sprintf('<div class="search-results">%s %s</div>', $search_results, $pager);

  //top seach header is above "search results"
  $ouptut .= $search_header_top;

  //Search header sticky and search results table
  $rows[] = array('data' => array($search_results));
  $header = array('data' => array('data' => $header_sticky, 'style' => 'width: 500px'));
  $output .= sprintf('<div class="clear"></div>%s', theme('table', $header, $rows, array('id' => 'search-results', 'style' => 'width: 500px')));
}
else {
  $output = sprintf('<div class="messages status">No Results Found for %s</div>', $searched_on);
}
print $output;

// printf('<div class="search-results-bottom">%s</div>', $output);