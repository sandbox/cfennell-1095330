<?php

function template_preprocess_apachesolr_cite_rows_to_show(&$variables) {
  $response = apachesolr_static_response_cache();
  $query = apachesolr_current_query();
  $keys = $query->get_query_basic();
  
  if (apachesolr_has_searched()) {
     if (empty($response)) {
       return;
     }
   }

   $rows = ($_GET['rows']) ? $_GET['rows'] : 10;
   $search_params = apachesolr_cite_get_search_params($values);
   $range  = array('10', '25', '50', '100');
   foreach ($range as $to_show) {
     if ($response->response->numFound >= $to_show && $response->response->numFound >= $to_show) {
       if ($to_show == $rows) {
         $output .= $to_show .' | ';
       }
       else  {
         $last_query = array_diff($_GET, array('rows' => $rows, 'q' => $_GET['q'], 'keys' => $_GET['keys']));
         $query = ($query) ? $last_query += array('rows' => $to_show) : array('rows' => $to_show);
         $output .= apachesolr_l($to_show, 'publications/'.  $keys, array('query' => $query)) . ' | ';
       }
     }
   }
  
  $variables['to_show'] = substr($output, 0, -3); 
}

